import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import { MainPage, DashboardPage } from './pages';

const Routes = () => (
  <div style={{ height: "100%" }}>
    <Switch>
      <Route exact path="/" component={MainPage} />
      <Route exact path="/login" />
      <Route path="/dashboard" component={DashboardPage} />
    </Switch>
  </div>
);

export default Routes;
