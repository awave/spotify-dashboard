import React from 'react'
import { Route, Redirect } from 'react-router-dom'

class PrivateRoute extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false
    }
  }
  
  render() {
    const { Component, ...rest } = this.props
    const { loggedIn } = this.state;
    return(
      <Route {...rest} render = { props => {
        loggedIn ? 
        <Component {...props} />
        : <Redirect to='/' /> 
      }} />
    )
  }
}

export default PrivateRoute
