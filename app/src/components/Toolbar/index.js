import React from 'react';
import { Link } from 'react-router-dom';
import './toolbar.css';

const Li = (props) => (
  <li>
    <Link to={props.to}>{props.children}</Link>
  </li>
);

class Toolbar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <nav className="toolbar">
        <ul>
          <Li to="/settings">Settings</Li>
          <Li to="/profile">Profile</Li>
          <Li to="/">Home</Li>
        </ul>
      </nav>
    );
  }
}

export default Toolbar;
