const authUrl = 'https://accounts.spotify.com/authorize';
const tokenUrl = 'https://accounts.spotify.com/api/token';

const accountsRoot = 'https://accounts.spotify.com';
const apiRoot = 'https://api.spotify.com/v1';

const api = path => `${apiRoot}${path}`;
const accounts = path => `${accountsRoot}${path}`;

export {
  api,
  accounts,
};
