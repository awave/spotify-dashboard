import * as env from 'dotenv';

env.config({ path: '../.env' });

const clientId: string = env.CLIENT_ID || process.env.CLIENT_ID;
const clientSecret: string = env.CLIENT_SECRET || process.env.CLIENT_SECRET;
const port: string = process.env.PORT || '8888';
const redirectUrl = `http://localhost:${port}/api/callback`;
const appAuthStateKey = 'spotify_auth_state';

const global = {
  clientId,
  clientSecret,
  port,
  redirectUrl,
  appAuthStateKey,
};

export default global;
